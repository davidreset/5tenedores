import React from 'react'
import { NavigationContainer } from '@react-navigation/native'
import { createBottomTabNavigator } from '@react-navigation/bottom-tabs'
import { Icon } from 'react-native-elements/dist/icons/Icon'
// import MaterialCommunityIcons from 'react-native-vector-icons/MaterialCommunityIcons';

import RestaurantStack from './RestaurantsStack'
import AccountStack from './AccountStack'
// import Restaurants from '../screens/Restaurants'
import Favorites from '../screens/Favorites'
import TopRestaurantes from '../screens/TopRestaurants'
import Search from '../screens/Search'
import Account from '../screens/Account/Account'

const Tab = createBottomTabNavigator();

export default function Navigation() {
  return (
    <NavigationContainer>
      <Tab.Navigator
        initialRouteName="restaurants"
        screenOptions={{
          tabBarInactiveTintColor: "#646464",
          tabBarActiveTintColor: "#00a680",
        }}
      >
        <Tab.Screen 
          name="restaurants" 
          component={RestaurantStack} 
          options={{ 
            title: 'Restaurantes',
            tabBarIcon: ({ color, size }) => (
              <Icon name="local-dining" color={color} size={size} />
            ),
          }} 
        />
        
        <Tab.Screen 
          name="favorites" 
          component={Favorites} 
          options={{ 
            title: 'Favoritos',
            tabBarIcon: ({ color, size }) => (
              <Icon name="favorite" color={color} size={size} />
            ),
          }} 
        />
        
        <Tab.Screen 
          name="top-restaurantes" 
          component={TopRestaurantes} 
          options={{ 
            title: 'Top 5',
            tabBarIcon: ({ color, size }) => (
              <Icon name="grade" color={color} size={size} />
            ),
          }} 
        />

        <Tab.Screen 
          name="search" 
          component={Search} 
          options={{ 
            title: 'Buscar',
            tabBarIcon: ({ color, size }) => (
              <Icon name="search" color={color} size={size} />
            ), 
          }} 
        />

        <Tab.Screen 
          name="account" 
          component={AccountStack} 
          options={{ 
            headerShown: false,
            title: 'Cuenta',
            tabBarIcon: ({ color, size }) => (
              <Icon name="person" color={color} size={size} />
            ), 
          }} 
        />
      </Tab.Navigator>
    </NavigationContainer>
  )
}
