import React from 'react';
import {StyleSheet, View, ScrollView, Text, Image} from 'react-native';
import { Button } from 'react-native-elements/dist/buttons/Button';
import { useNavigation } from '@react-navigation/core';

export default function UserGuest() {

  const navigation = useNavigation(); // declarar esta constante para el Login

  return (
    <ScrollView centerContent={true} style={styles.viewBody}>
      <Image 
        style={styles.image}
        source={require("../../../assets/img/user-guest.jpg")} 
        resizeMode="contain"
      />
      <Text style={styles.title}>Consulta tu perfil de 5 tenedores</Text>
      <Text style={styles.description}>
        The React Native docs should mention the Dimensions package on the front page - that it doesn't astounds me.
      </Text>
      <View style={styles.viewBtn}>
        <Button 
          title="Ver tu perfil"
          buttonStyle={styles.btnStyle}
          containerStyle={styles.btnContainer}
          onPress={() => navigation.navigate("login")}
        />
      </View>
    </ScrollView>
  )
}
 
const styles = StyleSheet.create({
  viewBody: {
    marginLeft: 30,
    marginRight: 30
  },
  image: {
    height: 300,
    width: "100%",
    marginTop: 40
  },
  title: {
    fontWeight: "bold",
    fontSize: 19,
    marginTop: 20,
    marginBottom: 10,
    textAlign: "center"
  },
  description: {
    textAlign: "center",
    marginBottom: 20
  },
  btnStyle: {
    backgroundColor: "#00a680"
  },
  viewBtn: {
    flex: 1,
    alignItems: "center"
  }
  ,
  btnContainer: {
    width: "70%"
  }
})