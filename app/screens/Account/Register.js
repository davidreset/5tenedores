import React from 'react'
import { StyleSheet ,View, Text, Image } from 'react-native'
import RegisterForm from '../../components/Account/RegisterForm'

export default function Register() {
  return (
    <View>
      <Image
        source={require("../../../assets/img/logo.png")} 
        resizeMode="contain"
        style={style.logo}
      />
      <View style={style.viewForm}>
        <RegisterForm />
      </View>
    </View>
  )
}

const style = StyleSheet.create({
  logo: {
    width: "100%",
    height: 150,
    marginTop: 20
  },
  viewForm: {
    marginLeft: 40,
    marginRight: 40
  }
}) 
