import React from 'react'
import { StyleSheet, Image, ScrollView, View, Text } from 'react-native'
import { Divider } from 'react-native-elements/dist/divider/Divider'
import { useNavigation } from '@react-navigation/core'

export default function Login() {
  return (
    <ScrollView>
      <Image
        source={require("../../../assets/img/logo.png")} 
        resizeMode="contain"
        style={style.logo}
      />
      <View style={style.viewContainer}>
        <Text>Login Form</Text>
        <CreateAccount />
      </View>
      <Divider style={style.divider} />
      <Text>Social Login</Text>
    </ScrollView>
  )
}

function CreateAccount() {
  const navigation = useNavigation()
  return (
    <Text style={style.textRegister}>
      Aun no tienes una cuenta?{" "}
      <Text onPress={() => navigation.navigate('register') } style={style.btnRegister}>Regístrate</Text>
    </Text>
  )
}

const style = StyleSheet.create({
  logo: {
    width: "100%",
    height: 150,
    marginTop: 20
  },
  viewContainer: {
    marginLeft: 40,
    marginRight: 40
  },
  textRegister: {
    marginTop: 15,
    marginLeft: 10,
    marginRight: 10
  },
  btnRegister: {
    color: "#00a680",
    fontWeight: "bold",
    paddingLeft: 20,
  },
  divider: {
    backgroundColor: "#00a680",
    margin: 40
  }
});