import firebase from 'firebase/app';
import 'firebase/auth';
import 'firebase/firestore';
// import { initializeApp } from "firebase/app";

const firebaseConfig = {
  apiKey: "AIzaSyBDYuXVeWU5D9UvsCrwZASTJzY2cSQyaUM",
  authDomain: "tenedores-992f6.firebaseapp.com",
  projectId: "tenedores-992f6",
  storageBucket: "tenedores-992f6.appspot. com",
  messagingSenderId: "686876391632",
  appId: "1:686876391632:web:cf2959e3fbd18c2f5446d5"
};

// export const firebaseApp = initializeApp(firebaseConfig);

 // Initialize Firebase
firebase.initializeApp(firebaseConfig);

const db = firebase.firestore();
const auth = firebase.auth()

export {db, auth};